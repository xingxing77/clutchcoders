Welcome to ClutchCoders!  
Every day each team member at least finish one problem.  
If anyone fails to submit one problem for two consective days, he will have to leave the group.  
Happy coding, everyone, fight for your dream job!  
Please fork and create a pull request to commit to the repo, thank you!  

11 Team Members: Accomplishments  
Qi Wang: 7  
Daisy: 6  
Snow Sun: 4  
Yu Zhang: 4  
Liz Tan: 4  
EthanL: 2  
Wei He: 2  
Jenny Zheng: 2  
Xing: 1  
Jessica: 1  
BoZhao Jin: 0  

3.31.2017 
Qi 349
Daisy 349
Yu 349
Liz 349

4.1.17
Qi 283 350
Yu 350
Snow 349 350
Liz 350
Daisy 94 144 145 173 350
Jenny 350
Xing 350
EthanL 349 350

4.2.17
Qi 21 88
Snow 88
Yu 88
Liz 88
Jessica 88
Wei 88 350

4.3.17
Qi 92 206
Snow 206
Jenny 206
Liz 206
Yu 206