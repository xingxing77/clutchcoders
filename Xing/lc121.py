class Solution(object):
    def maxProfit(self, prices):
        """
            :type prices: List[int]
            :rtype: int
            """
        if len(prices) < 2:
            return 0
        else:
            maxCurr = maxSofar = 0
            for i in range(1,len(prices)):
                maxCurr += prices[i] - prices[i-1]
                maxCurr = max(0,maxCurr)
                maxSofar = max(maxCurr,maxSofar)
            return maxSofar

