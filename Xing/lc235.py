# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def lowestCommonAncestor(self, root, p, q):
        """
            :type root: TreeNode
            :type p: TreeNode
            :type q: TreeNode
            :rtype: TreeNode
            """
        if root.val == p.val or root.val == q.val:
            return root
        elif p.val == q.val:
            temp = root
            while temp.val!= p.val:
                if temp.val <p.val:
                    temp = temp.right
                else:
                    temp = temp.left
            return temp
        else:
            big = max(p.val,q.val)
            small = min(p.val,q.val)
            temp = root
            while temp:
                if temp.val >= small and temp.val <= big:
                    return temp
                else:
                    if temp.val < small:
                        temp = temp.right
                    if temp.val > big:
                        temp = temp.left
            return temp


