# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def length(self,head):
        l = 0
        while head:
            l += 1
            head = head.next
        return l
    
    def getIntersectionNode(self, headA, headB):
        """
            :type head1, head1: ListNode
            :rtype: ListNode
            """
        if headA == None or headB == None:
            return None
        else:
            la = self.length(headA)
            lb = self.length(headB)
            
            if la > lb:
                while la > lb:
                    la -= 1
                    headA = headA.next
            if lb > la:
                while lb > la:
                    lb -= 1
                    headB = headB.next
            while headA and headB and headA != headB:
                headA = headA.next
                headB = headB.next
            return headA





