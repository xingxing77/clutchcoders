class Solution(object):
    def countBits(self, num):
        """
            :type num: int
            :rtype: List[int]
            """
        if num == 0:
            return [0]
        elif num == 1:
            return [0,1]
        else:
            ones = [0 for i in range(num+1)]
            ones[0] = 0
            ones[1] = 1
            power = 0
            for x in range(2,num+1):
                if x == 2 ** (power + 1):
                    power = power + 1
                    ones[x] = 1
                else:
                    ones[x] = ones[2**power] + ones[x-2**power]
            return ones

