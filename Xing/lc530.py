class Solution(object):
    def inorder(self,root):
        if root == None:
            return []
        else:
            result = []
            if root.left != None:
                result += self.inorder(root.left)
            result += [root.val]
            if root.right != None:
                result += self.inorder(root.right)
            return result

def getMinimumDifference(self, root):
    inorder = self.inorder(root)
        return min(inorder[i] - inorder[i-1] for i in range(1,len(inorder)))
