# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def maxDepth3(self, root): ## dfs
        if root == None:
            return 0
        else:
            maxDepth = 0
            height,stack = [1],[root]
            while stack:
                node = stack.pop()
                tempHeight = height.pop()
                maxDepth = max(tempHeight,maxDepth)
                if node.left != None:
                    stack.append(node.left)
                    height.append(tempHeight+1)
                if node.right != None:
                    stack.append(node.right)
                    height.append(tempHeight+1)
            return maxDepth

def maxDepth2(self, root): #recursive
    if root == None:
        return 0
    else:
        return 1+ max(self.maxDepth(root.left),self.maxDepth(root.right))
                
                
def maxDepth(self, root): ## level order
    if root == None:
        return 0
    else:
        depth = 0
        stack = [root]
        while stack:
            nextLevel = []
            depth += 1
            while stack:
                node = stack.pop()
                if node.left:
                    nextLevel.append(node.left)
                if node.right:
                    nextLevel.append(node.right)
            stack = nextLevel
        return depth




