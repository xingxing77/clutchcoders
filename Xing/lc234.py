# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def revertList(self,head):
        pre = None
        while head:
            current = head
            head = head.next
            current.next = pre
            pre = current
        return pre
    
    
    def isPalindrome(self, head):
        """
            :type head: ListNode
            :rtype: bool
            """
        if head == None or head.next == None:
            return True
        else:
            pre,slow,fast = None,head,head
            while fast and fast.next:
                pre = slow
                slow = slow.next
                fast = fast.next.next
            pre.next = None
            if fast != None:
                isEven = False
            else:
                isEven = True
            left = self.revertList(head)
            if not isEven:
                slow = slow.next
            while left and slow:
                if left.val != slow.val:
                    return False
                left = left.next
                slow = slow.next
            return True








