# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def inOrder(self,root):
        if root == None:
            return []
        else:
            result = []
            if root.left:
                result += self.inOrder(root.left)
            result += [root.val]
            if root.right:
                result += self.inOrder(root.right)
            return result

def kthSmallest(self, root, k):
    inorder = self.inOrder(root)
    return inorder[k-1]
