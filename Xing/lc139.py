class Solution(object):
    def wordBreak(self,s,words):
        match = [False] * len(s)
        for i in range(len(s)):
            for w in words:
                if s[i-len(w)+1:i+1] == w and (match[i-len(w)] == True or i-len(w) == -1):
                    match[i] = True
        return match[-1]
