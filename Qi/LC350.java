//350. Intersection of Two Arrays II
// Given two arrays, write a function to compute their intersection.

// Example:
// Given nums1 = [1, 2, 2, 1], nums2 = [2, 2], return [2, 2].

// Note:
// Each element in the result should appear as many times as it shows in both arrays.
// The result can be in any order.
// Follow up:
// What if the given array is already sorted? How would you optimize your algorithm?
// Ans: use two pointers approach
// What if nums1's size is small compared to nums2's size? Which algorithm is better?
// Ans: store num2's element into hashmap, then traverse nums1 to get intersection, then we will do less HashMap.containsKey operations. Assume constainsKey is always O(1), regarless of how large of the hashmap size
// What if elements of nums2 are stored on disk, and the memory is limited such that you cannot load all elements into the memory at once?
// Ans: If only nums2 cannot fit in memory, put all elements of nums1 into a HashMap, read chunks of array that fit into the memory, and record the intersections.
//If both nums1 and nums2 are so huge that neither fit into the memory, sort them individually (external sort), then read 2 elements from each array at a time in memory, record intersections (Two pointer approach).

import java.util.*;

public class LC350{

	public int[] intersect(int[] nums1, int[] nums2){ //O(n)
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>(); //use hashmap instead of hashset comparing to LC349
		ArrayList<Integer> list = new ArrayList<Integer>(); //use ArrayList instead of hashset comparing to LC350

		for(int num: nums1){
			if(map.containsKey(num)){
				map.put(num, map.get(num)+1); //the value of the map record how many times this num appears in one array
			}else{
				map.put(num, 1);
			}
		}

		for(int num: nums2){
			if(map.containsKey(num) && map.get(num) > 0){ //we decrease the value by one each time, so we can get all the repeated nums
				list.add(num);
				map.put(num, map.get(num)-1);
			}
		}

		int[] res = new int[list.size()];
		int k = 0;
		for(int num: list){
			res[k++] = num;
		}
		return res;
	}


	public int[] intersect2(int[] nums1, int[] nums2){ // O(nlogn)
		Arrays.sort(nums1); //pre sort cost O(nlogn)
		Arrays.sort(nums2);
		int i = 0;
        int j = 0;
        ArrayList<Integer> list = new ArrayList<Integer>();
        while( i < nums1.length && j < nums2.length){ //two pointer approach cost O(n)
            if( nums1[i] < nums2[j]){
                i ++;
            }else if(nums1[i] > nums2[j]){
                j ++;
            }else{
                list.add(nums1[i]); //instead of using hashset in LC349, we use an ArrayList here
                i ++;
                j ++;
            }
        }
        int[] res = new int[list.size()];
        int k = 0;
        for(int num : list){
            res[k++] = num;
        }
        return res;
	}

	public static void main(String[] args){
		int[] nums1 = {2,1};
		int[] nums2 = {1,1};
		LC350 l = new LC350();
		int[] res = l.intersect(nums1, nums2);
		System.out.println(Arrays.toString(res));
	}
}
